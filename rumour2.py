#!/usr/bin/python -tt

import sqlite3
from lang_processing import langProcess


def dbconnection():
	conn = sqlite3.connect('Project.db')
	results = conn.execute('SELECT * FROM TEMPMSG')
	results = results.fetchall()
	conn.close()
	return results


class step2():
	def __init__(self):
		self.output = {}
	
	def execute(self):
		sqlresults = dbconnection()
		doclist = dict()
		for result in sqlresults:
			doclist[result[0]] = result[3]
		self.lp = langProcess(doclist)
		for key in doclist:
			ranks = self.lp.process(key)
			self.output[key] = []
			sorted_words = sorted(ranks.items(), key=lambda x: x[1], reverse=True)
			for word, score in sorted_words:
				if score > 2.0:
					self.output[key].append(word.encode('utf8'))
				else:
					break
	
	def display(self, m=None):
		if not m:
			m = len(self.output)
		count = 0
		for key, values in self.output.iteritems():
			if count == m:
				break
			count += 1
			print "{:<5}  {}".format(key, ','.join(values))

	def getoutput(self):
		return self.output

	def getlp(self):
		return self.lp


if __name__ == '__main__':
	a = step2()
	a.execute()
	a.display()

	