#!/usr/bin/python -tt

import json


class step3():
	def __init__(self, s):
		self.inp = s.getoutput()
		self.lp = s.getlp()
		self.output = []

	def execute(self, d):
		result = self.dboperation(d)
		nodes = []
		for key, nodelist in result.iteritems():
			for node in nodelist['keywords']:
				nodes.append([nodelist['cat'], node])
		for i in xrange(len(nodes)):
			for j in xrange(i + 1, len(nodes)):
				cat1, node1 = nodes[i]
				node2 = nodes[j][1]
				pnode, cnode = self.processNodes(node1, node2)
				if pnode == cnode:
					continue
				if pnode == nodes[j][1]:
					nodes[i], nodes[j] = nodes[j], nodes[i]
			node = {'date': d, 'parent': pnode, 'child': cnode, 'edge': cat1, 'weight': 1}
			for n in self.output:
				if n['parent'] == node['parent'] and n['child'] == node['child'] and n['edge'] == node['edge']:
					n['weight'] += 1
					break
			else:
				for highernode in self.output:
					if node['child'] == highernode['parent']:
						break
				else:
					if node['parent'][0].isdigit() or node['child'][0].isdigit():
						continue
					self.output.append(node)
		self.output = self.output[:100]

	def display(self):
		print "{:<12}  {:<12}  {:<12}  {:<8}  {:<8}".format('Date', 'Parent', 'Child', 'Category', 'Weight')
		for node in self.output:
			print "{:<12}  {:<12}  {:<12}  {:<8}  {:<8}".format(node['date'], node['parent'],
				node['child'], node['edge'], node['weight'])

	def getoutput(self):
		return self.output

	def processNodes(self, node1, node2):
		try:
			freq = self.lp.n_containing
			r = freq(node1, node2) / freq(node2)
			if r > 0.5 and freq(node1) > freq(node2):
				return node1, node2
			else:
				return node2, node1
		except UnicodeDecodeError:
			return None, None

	def dboperation(self, d):
		self.date = d;
		import sqlite3
		conn = sqlite3.connect('Project.db')
		output = {}
		for key, value in self.inp.iteritems():
			query = "SELECT DATE, CATEGORY FROM TEMPMSG WHERE ID = '" + str(key) + "' AND DATE LIKE '" + d +"%'"
			res = conn.execute(query)
			res = res.fetchone()
			if res:
				output[key] = {'date': res[0], 'cat': res[1], 'keywords': value}
		conn.close()
		return output

	def saveoutput(self, filename):
		with open(filename, 'rb') as f:
			try:
				data = json.load(f)
			except:
				data = {}
		var = {self.date: self.output}
		data.update(var)
		with open(filename, 'wb') as f:
			json.dump(data, f)

if __name__ == '__main__': 
	from rumour2 import step2
	s2 = step2()
	s2.execute()
	s3 = step3(s2)
	s3.execute('5-1-2015')
	s3.display()
