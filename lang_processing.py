#!/usr/bin/python -tt

from __future__ import division, unicode_literals
import math
from textblob import TextBlob as tb


class langProcess:
	def __init__(self, dictlist):
		self.blobList = self.getBlobs(dictlist)
		self.length = len(self.blobList)
		self.restricted = ['sex', 'hard', 'cock', 'tit', 'tits', 'asian', 'porn', 'busty', 'boobs', 'cum',
						   'sexy', 'naked', 'blonde', 'hardcore', 'gorgeous', 'fucked', 'ahahahahha', 'its',
						   'fuck', 'fucking', 'sexheyxing', 'xxx', 'lesbian', 'teen', 'niggas', 'slut', 'http']

	def process(self, key):
		blob = self.blobList[key]
		scores = {word: self.ridf(word, blob) for word in blob.words}
		return scores

	def getWordCount(self, key):
		return len(self.blobList[key].words)

	def freq(self, blob, word):
		return blob.words.count(word)

	def getBlobs(self, dicts):
		temp = dict()
		for key in dicts:
			temp[key] = tb(dicts[key])
		return temp

	def tf(self, word, blob):
		return 0.5 + (0.5 * self.freq(blob, word) / max(self.freq(blob, w) for w in blob.words))

	def n_containing(self, word, word2=None):
		if word2:
			s = sum(1 for blob in self.blobList.values() if word in blob if word2 in blob)			
		else:
			s = sum(1 for blob in self.blobList.values() if word in blob)
		return s
	
	def idf(self, word):
		return math.log(self.length / (1 + self.n_containing(word)), 2)

	def tfidf(self, word, blob):
		return self.tf(word, blob) * self.idf(word)

	def ridf(self, word, blob):
		avg = 0
		for blob in self.blobList.values():
			avg += blob.words.count(word)
		avg /= self.length
		logValue = 1 - pow(math.e, -avg)
		return self.idf(word) - math.log(logValue, 2) - (10 if word.lower() in self.restricted or word.find('/') > -1 or len(word) < 4 else 0)

