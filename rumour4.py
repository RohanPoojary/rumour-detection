#!/usr/bin/python -tt

from os import popen


class step4():
	def __init__(self, s):
		self.input = s.getoutput()

	def execute(self):
		dotFile = """
		digraph G {
		rankdir=LR;
		"""
		words = set()
		for row in self.input:
			parent = row['parent']
			child = row['child']
			parent = parent.replace('-', '')
			child = child.replace('-', '')
			weight = row['weight']
			edge = row['edge']
			if not parent in words:
				dotFile += """
				{} [shape=ellipse];
				""".format(parent)
				words.add(parent)
			if not child in words:
				dotFile += """
				{} [shape=ellipse];
				""".format(child)
				words.add(child)
			dotFile += """
			{}->{} [weight={}; label="{}"];
			""".format(parent, child, weight, edge)
		dotFile += "}"
		f = popen('dot -Tpng -o graphWTA%s.png' % self.input[0]['date'], 'w')
		try:
			f.write(dotFile)
		except:
			raise BaseException('Error Creating Graph')
		finally:
			f.close()


if __name__ == '__main__':
	from rumour2 import step2

	s2 = step2()
	s2.execute()
	from rumour3 import step3

	s3 = step3(s2)
	s3.execute('5-1-2015')
	s4 = step4(s3)
	s4.execute()
