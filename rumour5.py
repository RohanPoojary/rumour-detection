#!/usr/bin/python2 -tt

import json
import matplotlib.pyplot as plt
import datetime


def calcgraphdiff(graph1, graph2):
	v1 = set()
	e1 = list()
	for node in graph1:
		v1.add(node['parent'])
		v1.add(node['child'])
		e1.append(node['edge'])
	v2 = set()
	e2 = list()
	for node in graph2:
		v2.add(node['parent'])
		v2.add(node['child'])
		e2.append(node['edge'])
	edgeintersection = 0
	for node1 in graph1:
		for node2 in graph2:
			if node1['parent'] == node2['parent']:
				if node1['child'] == node2['child']:
					edgeintersection += 1
	vert_diff = len(v1) + len(v2) - 2 * (len(v1.intersection(v2)))
	edge_diff = len(e1) + len(e2) - 2 * edgeintersection
	return vert_diff + edge_diff


class step5():

	def __init__(self, filename):
		self.filename = filename
		self.output = []

	def execute(self):
		with open(self.filename, 'rb') as f:
			data = json.load(f)
		tuples = []
		for key, val in data.iteritems():
			dt = key.split('-')
			tuples.append([datetime.date(int(dt[2]), int(dt[1]), int(dt[0])), val])
		tuples.sort(key=lambda x: x[0])
		output = dict()
		output[tuples[0][0]] = 0
		for i in xrange(1, len(tuples)):
			date, graph = tuples[i]
			val = calcgraphdiff(graph, tuples[i-1][1])
			output[date] = val
		self.output = output

	def getoutput(self):
		return self.output

	def displaygraph(self):
		dates = self.output.items()
		dates.sort(key=lambda x: x[0])
		yvals = []
		for date, val in dates:
			yvals.append(val)
		xvals = range(len(dates))
		plt.title('Final Graph Comparisions')
		plt.xlabel('Dates starting from ' + str(dates[0][0]))
		plt.ylabel('Edit Graph distance difference')
		plt.axis([0, len(xvals), min(yvals[1:]) - 100, max(yvals) + 100])
		plt.plot(xvals, yvals)
		plt.savefig('FinalGraph.png')


if __name__ == '__main__':
	s5 = step5('total.json')
	s5.execute()
	s5.displaygraph()
