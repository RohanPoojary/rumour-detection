#!/usr/bin/python -tt


class step6:
	def __init__(self, inp):
		self.input = inp.getoutput()
		self.output = []

	def execute(self):
		tuples = self.input.items()
		rumours = []
		tuples.sort(key=lambda x: x[0])
		for ind in xrange(1, len(tuples) - 1):
			prev = tuples[ind - 1]
			curr = tuples[ind]
			next = tuples[ind + 1]
			if curr[1] > prev[1] and curr[1] > next[1]:
				if curr[1] - prev[1] > 3 and curr[1] - next[1] > 3:
					rumours.append(curr)
		self.output = rumours

	def getoutput(self):
		return self.output


if __name__ == '__main__':
	from rumour5 import step5
	s5 = step5('total.json')
	s5.execute()
	s5.displaygraph()
	s6 = step6(s5)
	s6.execute()
	print s6.getoutput()
