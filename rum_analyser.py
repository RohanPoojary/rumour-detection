#!/usr/bin/python -tt

from rumour2 import step2
from rumour3 import step3
from rumour4 import step4
from rumour5 import step5
from rumour6 import step6


def main():

	filename = 'total.json'
	dates = ['1-1-2015', '2-1-2015', '3-1-2015', '4-1-2015', '5-1-2015',
			 '6-1-2015', '7-1-2015', '8-1-2015', '9-1-2015', '10-1-2015']

	print "Step 2 Started"
	s2 = step2()
	s2.execute()
	print "Step2 Finished"
	s2.display()

	for date in dates:
		print "Step3 Started for date:  %s" % date
		s3 = step3(s2)
		s3.execute(date)
		print "Step3 Finished"
		s3.saveoutput(filename)
		s3.display()

		print "Step4 Started for date: %s" % date
		s4 = step4(s3)
		s4.execute()
		print "Step4 Finished and Graph Has been created"

	print "Step5 Started"
	s5 = step5(filename)
	s5.execute()
	s5.displaygraph()
	print "Step5 Finished, Graph Has been created"

	print "Step6 Started"
	s6 = step6(s5)
	s6.execute()
	print "Step6 Ended"
	output = [str(node[0]) for node in s6.getoutput()]
	print 'Rumours spotted at ', ",".join(output)


if __name__ == '__main__':
	main()
